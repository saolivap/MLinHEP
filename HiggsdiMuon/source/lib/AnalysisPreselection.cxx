// System include(s):
#include <iostream>
#include <iomanip>

// Local include(s):
#include "AnalysisPreselection.h"

AnalysisPreselection::AnalysisPreselection(Option option)
{
  
  std::cout << std::endl;
  std::cout << "--- AnalysisPreselection :: Analysis on " << option;

  option_ = option;

  if ( option_ == sgn_ggF )  std::cout << ": Signal" <<std::endl;
  if ( option_ == bkg_pp )  std::cout << ": Background pp" <<std::endl;

}


AnalysisPreselection::~AnalysisPreselection()
{
  
  delete input;
  delete tree_in;
  delete output;
  delete tree_out;

}
 
void AnalysisPreselection::initialize()
{

  // Tree integer variables (no vectors) 
  n_events = 0;
  n_events_tree = 0;
  accepted_events = 0;
  n_muons = 0;
  
  leadpT_muon = -999;
  leadpT_muon_Charge = -999;
  leadpT_muon_Eta = -999;
  leadpT_muon_Phi = -999;
  subleadpT_muon = -999;
  subleadpT_muon_Charge = -999;
  subleadpT_muon_Eta = -999;
  subleadpT_muon_Phi = -999;

  mumu_Mass = -999;
  mumu_pT = -999;
  mumu_Et = -999;

  for ( int icount = 0; icount < n_counters; icount++ ) {
    count[icount] = 0;
  }
  
}

void AnalysisPreselection::addBranches()
{

  if ( option_ == sgn_ggF )  outname = filesFolder + "ggF_hmumu_Preselection.root";
  if ( option_ == bkg_pp  )  outname = filesFolder + "ppmumu_Preselection.root";
  
  output = new TFile( outname , "RECREATE");
  tree_out = new TTree("coupl", "coupl");
  tree_out->Branch("weight", &weight);
  tree_out->Branch("n_muons", &n_muons);
  tree_out->Branch("leadpT_muon", &leadpT_muon);
  tree_out->Branch("leadpT_muon_Charge", &leadpT_muon_Charge);
  tree_out->Branch("leadpT_muon_Eta", &leadpT_muon_Eta);
  tree_out->Branch("leadpT_muon_Phi", &leadpT_muon_Phi);
  tree_out->Branch("subleadpT_muon", &subleadpT_muon);
  tree_out->Branch("subleadpT_muon_Charge", &subleadpT_muon_Charge);
  tree_out->Branch("subleadpT_muon_Eta", &subleadpT_muon_Eta);
  tree_out->Branch("subleadpT_muon_Phi", &subleadpT_muon_Phi);
  tree_out->Branch("mumu_Mass", &mumu_Mass);
  tree_out->Branch("mumu_pT", &mumu_pT);
  tree_out->Branch("mumu_Et", &mumu_Et);

  if ( option_ == sgn_ggF )  fname = filesFolder + "ggF_hmumu_ntuple.root";
  if ( option_ == bkg_pp  )  fname = filesFolder + "ppmumu_ntuple.root";
  
  if (!gSystem->AccessPathName( fname )) {
    input = TFile::Open( fname ); // check if file in local directory exists
  } 
  
  if (!input) {
    std::cout << "ERROR: could not open data file" << std::endl;
    exit(1);
  }
  
  std::cout << "--- Using input file: " << input->GetName() << std::endl;
  if ( option_ == sgn_ggF )  std::cout << "--- Select Signal sample" << std::endl;
  if ( option_ == bkg_pp )   std::cout << "--- Select Background pp sample" << std::endl;

  // Load TTreeReader with input tree file
  tree_in = (TTree*)input->Get("coupl");
  tree_in->SetBranchAddress("weight", &weight);
  tree_in->SetBranchAddress("n_muons", &n_muons);
  tree_in->SetBranchAddress("leadpT_muon", &leadpT_muon);
  tree_in->SetBranchAddress("leadpT_muon_Charge", &leadpT_muon_Charge);
  tree_in->SetBranchAddress("leadpT_muon_Eta", &leadpT_muon_Eta);
  tree_in->SetBranchAddress("leadpT_muon_Phi", &leadpT_muon_Phi);
  tree_in->SetBranchAddress("subleadpT_muon", &subleadpT_muon);
  tree_in->SetBranchAddress("subleadpT_muon_Charge", &subleadpT_muon_Charge);
  tree_in->SetBranchAddress("subleadpT_muon_Eta", &subleadpT_muon_Eta);
  tree_in->SetBranchAddress("subleadpT_muon_Phi", &subleadpT_muon_Phi);

}

void AnalysisPreselection::finalize()
{

    // Events after cuts
  for (int icount=0; icount<n_counters; icount++) {
    
    TString Name = "Events after ";
    if (icount==0) Name +=  "Baseline cuts                        ";
    if (icount==1) Name +=  "min n.muons                          ";
    if (icount==2) Name +=  "min leading muon pT                ";
    if (icount==8) Name +=  "min dimuon Mass                          ";
    if (icount==9) Name +=  "max dimuon Mass                          ";
    
    std::cout<< Name + ": "<< count[icount] <<std::endl;
    
  }

  accepted_events = count[n_counters-1];
  std::cout<<"------------------------------------  " << std::endl;
  std::cout<<"Total number of events at 149 fb^-1: "<< weight*picoFemto*lum*(double)accepted_events <<std::endl;  

  // Write and close output file
  output->Write();
  output->Close();
  
  // finalize
  std::cout << "--- Finalize. " <<std::endl;
  
}

void AnalysisPreselection::execute()
{

  n_events_tree = tree_in->GetEntries();
  boost::progress_display show_progress( n_events_tree );
  std::cout << "--- Processing: " << n_events_tree << " events" << std::endl;
  
  for (Long64_t ievt=0; ievt < n_events_tree ;ievt++) {

    // Read branches per event
    tree_in->GetEntry(ievt);
    ++show_progress;

    TLorentzVector muon1, muon2;
    muon1.SetPtEtaPhiM(leadpT_muon, leadpT_muon_Eta, leadpT_muon_Phi, m_muon);
    muon2.SetPtEtaPhiM(subleadpT_muon, subleadpT_muon_Eta, subleadpT_muon_Phi, m_muon);
    
    TLorentzVector mumu = muon1 + muon2;
    mumu_Mass = mumu.M(); 
    mumu_pT = mumu.Pt();
    mumu_Et = mumu.Et();

    // Baseline selection
    count[0]+=1;
    
    // Minimum number of muons
    if ( n_muons < min_Nmuons ) continue;
    count[1]+=1;

    // Minimum leading photon pT
    if ( leadpT_muon < min_leadmuon_pT ) continue;
    count[2]+=1;

    // Minimum yy mass
    if ( mumu_Mass < min_Mmumu ) continue;
    count[3]+=1;

    // Maximum yy mass
    if ( mumu_Mass > max_Mmumu ) continue;
    count[4]+=1;

    // Fill output tree
    tree_out->Fill();
    
  }

  
  std::cout << "--- End of event loop " <<std::endl;   
}
