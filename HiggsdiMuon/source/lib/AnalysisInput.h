// Dear emacs, this is -*- c++ -*-
#ifndef FULLEXAMPLE_MYLIBRARY_H
#define FULLEXAMPLE_MYLIBRARY_H

// Local include(s):
#include "enums.h"

// ROOT includes(s):
#include "TMath.h"
#include "TTree.h"
#include "TFile.h"
#include "TSystem.h"

// Boost include(s):
#include <boost/program_options.hpp>
#include <boost/progress.hpp>

// Text include(s):
#include <fstream>

// Vector include(s):
#include <vector>

#include "TTreeReader.h"
#include "TTreeReaderArray.h"

class AnalysisInput {

public:
  
  AnalysisInput(Option option);
  ~AnalysisInput();
  void addBranches();
  void initialize();
  void finalize();
  void execute();

private:

  Option option_;
  TFile *output;
  TTree *tree_out;
  TFile *input;
  TTree *tree_in;
 
  // File name
  TString fname;
  TString outname;
  
  // Tree input variables

  // Weight 
  Float_t weight;

  // muon
  Float_t leadpT_muon;
  Float_t leadpT_muon_Eta;
  Float_t leadpT_muon_E;
  Float_t leadpT_muon_Phi;
  Float_t subleadpT_muon;
  Float_t subleadpT_muon_Eta;
  Float_t subleadpT_muon_E;
  Float_t subleadpT_muon_Phi;
  Int_t leadpT_muon_Charge;
  Int_t subleadpT_muon_Charge;

  // Number of events
  int n_events;
  int n_events_tree;

  // Number of objects
  Int_t n_muons;

  // Vectors taken from fReader
  Float_t muonpT[100];
  Float_t muonPhi[100];
  Float_t muonEta[100];
  Float_t muonCharge[100];
  
  // Tree files folder
  TString filesFolder = "/home/saolivap/WorkArea/samples/HiggsdiMuon/";

  // Read vectors from Input
  TTreeReader fReader;

  // Pico to femto
  int picoFemto = 1000;
  // Luminosity @ LHC Run-2 (fb-1)
  int lum = 140;
  
};

#endif
