// Dear emacs, this is -*- c++ -*-
#ifndef FULLEXAMPLE_MYLIBRARY_H
#define FULLEXAMPLE_MYLIBRARY_H

// Local include(s):
#include "enums.h"

// ROOT includes(s):
#include "TMath.h"
#include "TTree.h"
#include "TFile.h"
#include "TSystem.h"

// Boost include(s):
#include <boost/program_options.hpp>
#include <boost/progress.hpp>

// Text include(s):
#include <fstream>

// Vector include(s):
#include <vector>

#include "TLorentzVector.h"
#include "TVector3.h"


class AnalysisPreselection {

public:
  
  AnalysisPreselection(Option option);
  ~AnalysisPreselection();
  void addBranches();
  void initialize();
  void finalize();
  void execute();

private:

  Option option_;
  TFile *output;
  TTree *tree_out;
  TFile *input;
  TTree *tree_in;
 
  // File name
  TString fname;
  TString outname;
  
  // Tree input variables

  // Weight 
  Float_t weight;

  // muon
  Float_t leadpT_muon;
  Float_t leadpT_muon_Eta;
  Float_t leadpT_muon_E;
  Float_t leadpT_muon_Phi;
  Float_t subleadpT_muon;
  Float_t subleadpT_muon_Eta;
  Float_t subleadpT_muon_E;
  Float_t subleadpT_muon_Phi;
  Int_t leadpT_muon_Charge;
  Int_t subleadpT_muon_Charge;
  Float_t mumu_Mass;
  Float_t mumu_pT;
  Float_t mumu_Et;

  // Number of events
  int n_events;
  int n_events_tree;
  int accepted_events;

  // Number of objects
  Int_t n_muons;
  
  // Tree files folder
  TString filesFolder = "/home/saolivap/WorkArea/samples/HiggsdiMuon/";

  // Pico to femto
  int picoFemto = 1000;
  // Luminosity @ LHC Run-2 (fb-1)
  int lum = 140;

  // Counter events
  int count[5];
  int n_counters = 5;

  // Masses
  float m_muon = 0.105;
  
  // Object Cuts
  int min_Nmuons = 2;
  float min_Mmumu = 105;
  float max_Mmumu = 160;
  float min_leadmuon_pT = 35;
  
};

#endif
