// System include(s):
#include <iostream>
#include <iomanip>

// Local include(s):
#include "AnalysisInput.h"

AnalysisInput::AnalysisInput(Option option)
{
  
  std::cout << std::endl;
  std::cout << "--- AnalysisInput :: Analysis on " << option;

  option_ = option;

  if ( option_ == sgn_ggF )  std::cout << ": Signal" <<std::endl;
  if ( option_ == bkg_pp )  std::cout << ": Background pp" <<std::endl;

}


AnalysisInput::~AnalysisInput()
{
  
  delete input;
  delete tree_in;
  delete output;
  delete tree_out;

}
 
void AnalysisInput::initialize()
{

  // Tree integer variables (no vectors) 
  n_events = 0;
  n_events_tree = 0;

  leadpT_muon = -999;
  leadpT_muon_Charge = -999;
  leadpT_muon_Eta = -999;
  leadpT_muon_Phi = -999;
  subleadpT_muon = -999;
  subleadpT_muon_Charge = -999;
  subleadpT_muon_Eta = -999;
  subleadpT_muon_Phi = -999;
  
}

void AnalysisInput::addBranches()
{

  if ( option_ == sgn_ggF )  outname = filesFolder + "ggF_hmumu_ntuple.root";
  if ( option_ == bkg_pp  )  outname = filesFolder + "ppmumu_ntuple.root";
  
  output = new TFile( outname , "RECREATE");
  tree_out = new TTree("coupl", "coupl");
  tree_out->Branch("weight", &weight);
  tree_out->Branch("n_muons", &n_muons);
  tree_out->Branch("leadpT_muon", &leadpT_muon);
  tree_out->Branch("leadpT_muon_Charge", &leadpT_muon_Charge);
  tree_out->Branch("leadpT_muon_Eta", &leadpT_muon_Eta);
  tree_out->Branch("leadpT_muon_Phi", &leadpT_muon_Phi);
  tree_out->Branch("subleadpT_muon", &subleadpT_muon);
  tree_out->Branch("subleadpT_muon_Charge", &subleadpT_muon_Charge);
  tree_out->Branch("subleadpT_muon_Eta", &subleadpT_muon_Eta);
  tree_out->Branch("subleadpT_muon_Phi", &subleadpT_muon_Phi);

  if ( option_ == sgn_ggF  )  fname = filesFolder + "MadGraph5_Pythia8_Delphes_ggF_hmumu_10kEv.root";
  if ( option_ == bkg_pp )  fname = filesFolder + "MadGraph5_Pythia8_Delphes_ppmumu_100kEv.root";
  
  if (!gSystem->AccessPathName( fname )) {
    input = TFile::Open( fname ); // check if file in local directory exists
  } 
  
  if (!input) {
    std::cout << "ERROR: could not open data file" << std::endl;
    exit(1);
  }
  
  std::cout << "--- Using input file: " << input->GetName() << std::endl;
  if ( option_ == sgn_ggF )  std::cout << "--- Select Signal sample" << std::endl;
  if ( option_ == bkg_pp )   std::cout << "--- Select Background pp sample" << std::endl;

  // Load TTreeReader with input tree file
  tree_in = (TTree*)input->Get("Delphes");
  fReader.SetTree(tree_in);

}

void AnalysisInput::finalize()
{

  // Write and close output file
  output->Write();
  output->Close();
  
  // finalize
  std::cout << "--- Finalize. " <<std::endl;
  
}

void AnalysisInput::execute()
{

  // Variables input-reader
  TTreeReaderArray<Float_t> r_weight(fReader, "Event.Weight");
    
  TTreeReaderArray<Float_t> r_muonpT(fReader, "Muon.PT");
  TTreeReaderArray<Float_t> r_muonPhi(fReader, "Muon.Phi");
  TTreeReaderArray<Float_t> r_muonEta(fReader, "Muon.Eta");
  TTreeReaderArray<Int_t> r_muonCharge(fReader, "Muon.Charge");
    
  // Boost - Progress bar
  n_events_tree = tree_in->GetEntries();
  boost::progress_display show_progress( n_events_tree );
  
  // Event Loop
  while (fReader.Next()) {

    n_events++;
    ++show_progress;

    // std::cout << "EVENT " << n_events << std::endl;

    //initialise muon
    std::size_t leading = 0;
    n_muons = 0;
    muonCharge[100] = { 0 };
    muonPhi[100] = { 0 };
    muonEta[100] = { 0 };
    muonpT[100] = { 0 };
    // Muon Loop
    for (unsigned long int imuon = 0; imuon < r_muonpT.GetSize(); ++imuon ) {
      // std::cout << "Muon " << imuon << " with pT = " << r_muonpT[imuon] << std::endl;

      n_muons++;
      muonpT[imuon] = r_muonpT[imuon];
      muonCharge[imuon] = r_muonCharge[imuon];
      muonPhi[imuon] = r_muonPhi[imuon];
      muonEta[imuon] = r_muonEta[imuon];

      if ( muonpT[leading] < muonpT[imuon] ) {
	leading = imuon;
      }
      
    } // muon loop

    // Fill leading and sub-leading muon
    if ( n_muons > 0 ) leadpT_muon = muonpT[leading];
    if ( n_muons > 0 ) leadpT_muon_Charge = muonCharge[leading];
    if ( n_muons > 0 ) leadpT_muon_Eta = muonEta[leading];
    if ( n_muons > 0 ) leadpT_muon_Phi = muonPhi[leading];
    if ( n_muons > 1 ) subleadpT_muon = muonpT[leading+1];
    if ( n_muons > 1 ) subleadpT_muon_Charge = muonCharge[leading+1];
    if ( n_muons > 1 ) subleadpT_muon_Eta = muonEta[leading+1];
    if ( n_muons > 1 ) subleadpT_muon_Phi = muonPhi[leading+1];

    // if ( n_muons > 0 ) std::cout << "Leading pT Muon: " << leadpT_muon << '\n' ;
    // if ( n_muons > 1 ) std::cout << "Sub-leading pT Muon: " << subleadpT_muon << '\n' ;
        
    // initialise event
    weight = 0;
    // Event Loop
    for (unsigned long int ievt = 0; ievt < r_weight.GetSize(); ++ievt ) {
      //      std::cout << "Weight " << r_weight[ievt] << std::endl;      
      weight = r_weight[ievt];

    } // event loop

    // fill events without selection cuts
    tree_out->Fill();
    
  } // event loop
  
  std::cout << "--- End of event loop " <<std::endl;   
}
