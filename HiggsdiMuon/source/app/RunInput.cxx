// System include(s):
#include <iostream>

// Local include(s):
#include "AnalysisInput.h"

// ROOT include(s):
#include "TTree.h"

int Run( Option opt ) {

  AnalysisInput *Higgs = new AnalysisInput(opt);
  Higgs->initialize();
  Higgs->addBranches();
  Higgs->execute();
  Higgs->finalize();
  
  // Return gracefully:
  return 0;
}

int main( int argc, char* argv[] ) {

  Run(sgn_ggF);
  Run(bkg_pp);
  
  // Return gracefully:
  return 0;
  
}
