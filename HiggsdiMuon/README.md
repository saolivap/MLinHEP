# HiggsdiMuon

Experimental search for the Higgs boson decaying to di-muons with the ATLAS detector at Run-2 (13 TeV, 140 fb-1).

MonteCarlo (MC) samples are created with MadGraph5, using Pythia8 for hadronisation, and Delphes as detector simulation.

This project is part of the Machine Learning in High Energy Physics course.

--------------------------------------------------------------------------------------------------------

The project is divided in a set of scripts that uses Delphes root output dataset as input file.

AnalysisInput
--------------------
* Read the input file and create a ntuple with new kinematic variables 

AnalysisPreselection
--------------------
* Read the ntuple to create new variables (TLorentz vectors) and pre-selection cuts

DrawHistCompare
--------------------
* Takes the output tree file from AnalysisPreselection and makes plots from it (for signal and background). 

contact:
* Richards Gonzalez: rigonzal@cern.ch 
* Michael Haacke: mhaacke@cern.ch
* Sebastian Olivares: saolivap@cern.ch
* Jilberto Zamora: jilberto.zamora@unab.cl
