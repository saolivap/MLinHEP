import ROOT
ROOT.gROOT.SetBatch(True)

import os
cwd = os.getcwd()
stylepath = cwd + '/AtlasStyle/'
print(stylepath)

ROOT.gROOT.LoadMacro( stylepath + "AtlasStyle.C")
ROOT.gROOT.LoadMacro( stylepath + "AtlasLabels.C")
ROOT.gROOT.LoadMacro( stylepath + "AtlasUtils.C")
ROOT.SetAtlasStyle()

#Flags
doLogY = False
doLogX = False
Normalise = True

basepath = '/home/saolivap/WorkArea/samples/HiggsdiMuon/'
tree = 'coupl'
hists = ['mumu_Mass']
stacks = ['hs', 'hs_merge']
samples = {
    'bkg_pp': [basepath + 'ppmumu_Preselection.root'],
    'sgn_ggF': [basepath + 'ggF_hmumu_Preselection.root'],
}
#merges = ['AllBkg']

# CREATING HISTOGRAMS
h = {}
for sample in samples.keys():
    h[sample] = {}

    for hist in hists:
        h[sample][hist] = {}

        n = 'h_%s_%s' % (sample, hist)

        if hist == 'mumu_Mass':
            h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s" % (hist, sample), 50, 105, 150)

#FILLING
for hist in hists:
    print ('processing histogram: %s' % hist)

    for sample in samples.keys():
        print ('processing sample: %s' % sample)
        l = samples[sample]

        for fname in l:
            print ('            > %s' % fname)
            f = ROOT.TFile.Open(fname)
            t = f.Get(tree)

            if hist == 'mumu_Mass': n = 'htmp(50,105,150)'
            
            if t.Draw('%s>>%s' % (hist, n),
                      '1*weight*140*1000',  #1*weight
                      'goff'
            ):
                h[sample][hist].Add(ROOT.gDirectory.Get('htmp'), 1.)

del f

# NORMALISE & CONFIG
for sample in samples.keys():
    for hist in hists:

        if Normalise==True:
            norm = 1;
            if h[sample][hist].Integral() > 0:
                scale = norm/h[sample][hist].Integral()
                h[sample][hist].Scale(scale)
                                
        # Style and Colour
        h[sample][hist].SetLineWidth(2) 
        h[sample][hist].SetLineStyle(1)
        h[sample][hist].SetFillStyle(3004)
        h[sample][hist].SetLineColor(ROOT.kBlack)
        if ( sample == 'sgn_ggF' ) :
            h[sample][hist].SetFillColor(ROOT.kOrange+10)
            h[sample][hist].SetLineColor(ROOT.kOrange+1)
        if ( sample == 'bkg_pp' ) :
            h[sample][hist].SetFillColor(ROOT.kAzure)
            h[sample][hist].SetLineColor(ROOT.kAzure-4)
        
# PLOTTING
for hist in hists:

    # Style and colour
#    h['bkg_pp'][hist].SetLineColor(ROOT.kRed)
#    h['sgn_ggF'][hist].SetLineColor(ROOT.kBlue)

    # Plot title
    h['sgn_ggF']['mumu_Mass'].GetXaxis().SetTitle('Mass [GeV]')

    # Tex in canvas
    tex = ROOT.TLatex(0.2, 0.85, "ML in HEP")
    tex.SetName('tex')
    tex.SetNDC(True)
    tex.SetTextSize(0.04)
    # Tex in canvas - luminosity
    texlum = ROOT.TLatex(0.2, 0.8, "#sqrt{s} = 13 TeV, L = 149 fb^{-1}")
    texlum.SetName('tex')
    texlum.SetNDC(True)
    texlum.SetTextSize(0.03)
    texSR = ROOT.TLatex(0.2, 0.76, "SR: Baseline selection")
    texSR.SetName('tex')
    texSR.SetNDC(True)
    texSR.SetTextSize(0.02)

    
    #legend configuration 
    xmin = 0.68
    xmax = 0.84
    ymin = 0.78
    ymax = 0.90

    legend = ROOT.TLegend(xmin, ymin, xmax, ymax)
    legend.SetFillColor(0)
    legend.SetBorderSize(0)
    legend.SetFillStyle(0)
    legend.SetTextFont(42)
    legend.SetTextSize(0.035)
    legend.SetName('couplings')
    legend.SetShadowColor(0)

    legend.AddEntry(h['bkg_pp'][hist],'pp #rightarrow #mu#mu', 'f')
    legend.AddEntry(h['sgn_ggF'][hist],'ggF #rightarrow H #rightarrow #mu#mu', 'f')

    # THStack  
    hs  = ROOT.THStack("hsStack","Histogram Comparison")

    hs.Add(h['bkg_pp'][hist])
    hs.Add(h['sgn_ggF'][hist])

    c = ROOT.TCanvas('c', 'c', 800, 800)
    if doLogY == True: c.SetLogy()
    if doLogX == True: c.SetLogx()
    hs.Draw('nostack, hist')

    # Axis title
    hs.GetYaxis().SetTitle('Events')
    if hist == 'particleID': hs.GetXaxis().SetTitle('m_{#mu #mu}')
    
    legend.Draw()
    tex.Draw()
    texlum.Draw()
    texSR.Draw()    
    c.Update()

    if (doLogX == True): c.SaveAs( cwd + '/plotsCompare/Log/%s.png' % (hist))
    else: c.SaveAs( cwd + '/plotsCompare/Default/%s.png' % (hist))




